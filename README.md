# St[IA]mmtisch

Le jeu St[IA]mmtisch, imaginé par la DRANE Grand Est, est inspiré de la tradition du Stammtisch : il s’agit avant tout de se retrouver pour partager de bons moments, autour d’un sujet d’actualité : l’intelligence artificielle (IA). L’IA, on peut en parler sans être connecté !

Il s'agit d'un jeu de plateau, prévu pour 2 à 6 joueurs, ou équipes, accompagnés d'un animateur. La durée d'une partie, d'environ 50 minutes, a été calibrée pour une utilisation en classe. Le jeu peut toutefois être utilisé aussi bien entre adultes qu'avec des élèves.

![Photographie présentant le matériel de jeu du StIAmmtisch](https://dane.site.ac-strasbourg.fr/wp-content/uploads/2024/05/Screenshot-2024-05-14-at-08-13-44-stIAmmtisch-presentation-CREIA.pptx.png)

## Questions et assistance
Pour toute question relative au jeu ou à son fonctionnement, vous pouvez contacter l'équipe de création à l'adresse [stiammtisch[at]ac-strasbourg.fr](mailto:stiammtisch@ac-strasbourg.fr)

## Feuille de route
Actuellement les niveaux junior et intermédiaire sont disponibles. Un niveau expert est envisagé depuis la création du jeu mais n'est pas disponible à ce jour.

Un travail régulier de maintien à jour des questions est nécessaire en raison de la rapide évolution des systèmes d'intelligence artificielle.

## Contribuer
Toute contribution au projet est la bienvenue. Vous pouvez proposer de nouvelles questions en nous contactant à l'adresse [stiammtisch[at]ac-strasbourg.fr](mailto:stiammtisch@ac-strasbourg.fr).

Les remarques concernant le contenu mis en ligne peuvent être transmise via les tickets.

## Les auteurs
Le matériel de jeu a été conçu par les équipes de la Drane Grand Est.

## Licence
Le jeu St[IA]mmtisch © 2024 a été conçu par la [Drane Grand Est](https://dane.site.ac-strasbourg.fr/) et est placé sous [licence CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/). 

Cette licence s'applique à tous les supports du jeu, sans préjudice des droits de [Freepik](https://www.flaticon.com/fr/auteurs/freepik) sur ses illustrations utilisées pour certains contenus imprimables. Ces illustrations sont placées sous [licence Flaticon](https://www.flaticon.com/fr/legal#nav-flaticon-agreement).