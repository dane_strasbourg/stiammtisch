# St[IA]mmtisch - Les règles du jeu

👥 2 à 6 joueurs ou équipes + 1 animateur. Maximum recommandé : 6 équipes de 3 joueurs.

🕐 Durée d'une partie 45 min (hors explication des réponses)

Âge : à partir de 9 ans

## Aperçu et but du jeu

Lancez-vous dans la course à l’IA ! Les plus grands spécialistes de l’IA se retrouvent régulièrement au St[IA]mmtisch pour débattre et surveiller l’avancée des connaissances de leurs concurrents.
Pour s’imposer, il faut exceller dans 7 domaines :

- Généralités 
- Histoire 
- Défi 
- Débat 
- Éducation 
- Déconnexion 
- Sécurité 

Le vainqueur est le joueur, ou l’équipe, qui atteint la case St[IA]mmtisch en premier.

## Matériel

- 1 plateau de jeu 
- 6 pions 
- 6 cartes joueur/équipe
- 1 carte IA 
- 2 lots de 56 cartes : niveau junior (dès 9 ans) et niveau intermédiaire
- 12 cartes spéciales
- 1 dé à six faces (non fourni) numéroté de 1 à 3


## Mise en place

1. Placez la carte IA au centre de la table. Elle doit être accessible rapidement pour toutes les équipes. 
2. Placez le plateau de jeu sur la table. 
3. Choisissez un niveau de jeu pour la partie, en fonction de l’âge des joueurs et de leur niveau d’acculturation à l’IA.
4. Prenez le lot de cartes du niveau choisi et séparez les cartes en fonction de leur thème (couleur du verso).
5. Disposez les 7 pioches, face cachée, à proximité de l’animateur.
6. Placez le pion de chaque équipe sur l’une des cases départ. 
7. Chaque équipe place sa carte Équipe devant elle. 

## Déroulement de la partie

Chaque joueur lance le dé. Le joueur ayant obtenu le meilleur score commence. Le jeu se déroule ensuite dans le sens des aiguilles d’une montre.

### Déroulement d’un tour de jeu

- Le joueur lance le dé et avance son pion du nombre de cases indiqué par le dé.
- Si le joueur tombe sur une case colorée, l’animateur prend la première carte de la pioche de cette couleur et lit la question.
    - Si le joueur donne la bonne réponse, il avance son pion d’une case.
    - Si le joueur donne une mauvaise réponse, il recule son pion d’une case. [autre option : rien ne se passe]
- Pour les cartes “Débat”, tous les joueurs sont encouragés à prendre la parole,  l’équipe avance d’une case quelques soient les réponses.
- Si le joueur tombe sur la case ⌛, les joueurs prennent en main leur carte joueur. L’animateur décompte de 3 à 0. Lorsqu’il arrive à 0, les joueurs posent le plus rapidement possible leur carte joueur sur la carte IA. Le joueur ayant été le plus rapide peut piocher une carte spéciale.
La carte spéciale peut être jouée immédiatement ou conservée pour une autre occasion.
- Si le chiffre indiqué par le dé permettrait d’aller au-delà de la case St[IA]mmtisch, le joueur recule son pion du nombre de cases surnuméraires.

### Cartes spéciales

- Carte Virus informatique (usage unique) : à utiliser envers un autre joueur. Le pion du joueur ciblé retourne à la case départ.
- Carte Coupure d’électricité (usage unique) : à utiliser envers un autre joueur. Le joueur ciblé recule son pion de 2 cases. Si le pion arrive sur une case ⌛, appliquez la règle de cette case.
- Carte Serveur miroir (usage unique) : permet de se protéger de la carte Virus informatique.
- Carte Onduleur (usage unique) : permet de se protéger de la carte Coupure d’électricité.
- Carte Saut technologique (usage unique) : le joueur avance son pion de 2 cases. Si le pion arrive sur une case ⌛, appliquez la règle de cette case.
- Carte Agent infiltré (usage unique) : le joueur peut obtenir un indice pour la question de son choix.

### Fin de partie

Lorsqu’un joueur arrive sur la case St[IA]mmtisch après avoir répondu à une question ou en arrivant sur la case après un jet de dé, il emporte la partie.

[🏠 Revenir à la page d'accueil](https://dane_strasbourg.forge.apps.education.fr/stiammtisch/)