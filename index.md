# St[IA]mmtisch
## Un jeu pour acculturer à l'intelligence artificielle

Le jeu St[IA]mmtisch, imaginé par la DRANE Grand Est, est inspiré de la tradition du Stammtisch : il s’agit avant tout de se retrouver pour partager de bons moments, autour d’un sujet d’actualité : l’intelligence artificielle (IA). L’IA, on peut en parler sans être connecté !

Il s'agit d'un jeu de plateau, prévu pour 2 à 18 joueurs, accompagnés d'un animateur. La durée d'une partie, d'environ 50 minutes, a été calibrée pour une utilisation en classe. Le jeu peut toutefois être utilisé aussi bien entre adultes qu'avec des élèves.

![Photographie présentant le matériel de jeu du StIAmmtisch](https://dane.site.ac-strasbourg.fr/wp-content/uploads/2024/05/Screenshot-2024-05-14-at-08-13-44-stIAmmtisch-presentation-CREIA.pptx.png)

:::success
Le jeu St[IA]mmtisch a fait l'objet d'un article de presse paru le 21 janvier 2025 dans les *Dernières Nouvelles d'Alsace*. L'article, réservé aux abonnés, est accessible via [ce lien](https://www.dna.fr/culture-loisirs/2025/01/21/un-jeu-de-plateau-sur-l-ia-et-le-numerique-pour-sensibiliser-les-lyceens).
:::

[Découvrir les règles du jeu](https://dane_strasbourg.forge.apps.education.fr/stiammtisch/regles)

<details><summary>Télécharger le matériel de jeu</summary>
Le matériel de jeu est librement téléchargeable sur la forge des communs numériques éducatifs. Les niveaux junior et intermédiaire sont téléchargeables sous forme d'archives prêtes à imprimer.

[Télécharger le niveau junior prêt à imprimer](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/raw/main/StIAmmtisch_junior_PnP.zip?ref_type=heads)

[Télécharger le niveau intermédiaire prêt à imprimer](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/raw/main/StIAmmtisch_intermediaire_PnP.zip?ref_type=heads)

Vous pouvez également télécharger les fichiers en format ouvert afin de les modifier ou les adapter :
- [les règles du jeu](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/blob/main/R%C3%A8gles_du_jeu_StIAmmtisch.odt?ref_type=heads).
- [le plateau de jeu](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/blob/main/Plateau_StIAmmtisch.svg?ref_type=heads) à imprimer en couleur au format A3.
- [les cartes équipe](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/blob/main/Cartes_%C3%A9quipes_StIAmmtisch.svg?ref_type=heads).
- [les jetons équipe](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/blob/main/Jetons_%C3%A9quipes_StIAmmtisch.svg?ref_type=heads) en papier ou, si vous disposez d'une imprimante 3D, des jetons dont les fichiers stl se trouvent dans [le dépôt du jeu](https://forge.apps.education.fr/dane_strasbourg/stiammtisch).
- [la carte IA](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/blob/main/Carte_IA_StIAmmtisch.svg?ref_type=heads)
- [les cartes spéciales](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/blob/main/Cartes_StIAmmtisch_-_sp%C3%A9ciales.odg?ref_type=heads)
- les cartes questions de niveau [junior](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/blob/main/Cartes_StIAmmtisch_-_niv_junior.odg?ref_type=heads) ou [intermédiaire](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/blob/main/Cartes_StIAmmtisch_-_niv_intermediaire.odg?ref_type=heads).
- les réponses aux questions niveau [junior](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/blob/main/StIAmmtisch_r%C3%A9ponses_niveau_junior.odt?ref_type=heads) ou [intermédiaire](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/blob/main/StIAmmtisch_r%C3%A9ponses_niveau_intermediaire.odt?ref_type=heads).
</details>

<details>
<summary>Contact et assistance</summary>
Pour toute question relative au jeu ou à son fonctionnement, vous pouvez contacter l'équipe de création via les [tickets](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/issues) ou en écrivant à l'adresse [stiammtisch[at]ac-strasbourg.fr](mailto:stiammtisch@ac-strasbourg.fr)
</details>

<details><summary>Feuille de route</summary>
Actuellement les niveaux junior et intermédiaire sont disponibles. Un niveau expert est envisagé depuis la création du jeu mais n'est pas disponible à ce jour.

Un travail régulier de maintien à jour des questions est nécessaire en raison de la rapide évolution des systèmes d'intelligence artificielle.

Une nouvelle édition du jeu est en cours d'élaboration. Elle permettra de faciliter la prise en main du jeu sans animateur grâce à des réponses placées au dos des cartes.
</details> 
<details><summary>Contribuer</summary>
Toute contribution au projet est la bienvenue. 

Vous pouvez proposer de nouvelles questions en nous contactant à l'adresse [stiammtisch[at]ac-strasbourg.fr](mailto:stiammtisch@ac-strasbourg.fr).

Les remarques concernant le contenu mis en ligne peuvent être transmises via les [tickets](https://forge.apps.education.fr/dane_strasbourg/stiammtisch/-/issues).
</details>
<details><summary>Licence</summary>
Le jeu St[IA]mmtisch © 2024 a été conçu par la [Drane Grand Est](https://dane.site.ac-strasbourg.fr/) et est placé sous [licence CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/). 

Cette licence s'applique à tous les supports du jeu, sans préjudice des droits de [Freepik](https://www.flaticon.com/fr/auteurs/freepik) sur ses illustrations utilisées pour certains contenus imprimables. Ces illustrations sont placées sous [licence Flaticon](https://www.flaticon.com/fr/legal#nav-flaticon-agreement).
</details>